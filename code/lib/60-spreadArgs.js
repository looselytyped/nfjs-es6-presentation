//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions_and_function_scope/rest_parameters

clear();
function something(a, ...b) {
    return [a, b.map(n => n * 2)];
}

console.log(something(1, 2, 3, 4)); // [1, [4, 6, 8]]

// demise of the 'arguments'
/*
  'arguments' is NOT a real array
  '...' gives you a real array to work with
**/

function sortArgs() {
    return arguments.sort(); // ERROR
}

sortArgs(3,2,1);

function sortArgs(...args) {
    return args.sort();
}

sortArgs(3,2,1);
