//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_operator
clear();
var arr = [1,2,3];

console.log(arr);
console.log(...arr);

// useful where you have multiple arity functions
var other = [4,5,6];
other.push(...arr);
console.log(other);

// great for concatenating arrays
var combo = [10, 9, 7, ...other];
console.log(combo);
