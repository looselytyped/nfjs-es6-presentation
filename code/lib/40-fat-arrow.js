//==================================================
//==================================================
// single args don't need parentheses
clear();
var fatOne = val => { return val };
console.log(fatOne(10));

// multiple args have to have parentheses
clear();
var fatTwo = (a, b) => { return [a, b] }
console.log(fatTwo('js', 'next'));

// you can skip the curly brackets for simple definitions
// If you do the 'return' is implicit
clear();
var fatThree = val =>  val ;
console.log(fatThree(10)); //10

var fatFour = (a, b) =>  [a, b]
console.log(fatFour('js', 'next'));

// no args have to have parentheses
clear();
var fatFive = () => 'I have no args';
console.log(fatFive());

//
//==================================================
//==================================================
// fat-arrow functions DO NOT define a new 'this'
// They PERMANENTLY bind to the 'this' that exists when they are defined
clear();

function Person(name) {
    this.name = name;
    this.sayHello = function() {
        return "Hello! My name is " + this.name; // This is the new object
    }
}

var raju = new Person("raju");
console.log(raju.sayHello());

function invoke(method) {
    console.log(method());
}

invoke(raju.sayHello); // Hello! My name is

function Person(name) {
    this.name = name;
    this.sayHello = () => "Hello! My name is " + this.name;
}

var betterRaju = new Person("raju");
console.log(betterRaju.sayHello());
invoke(betterRaju.sayHello); // Hello! My name is raju

// CANNOT Change 'this'
var obj = {};
betterRaju.sayHello.call(obj); // "Hello! My name is raju"


//==================================================
//==================================================
// 'strict' mode works like you would expect it to
clear();

var x = function () {
    return () => this;
}

console.dir(x()());

var x = function () {
    'use strict';
    return () => this;
}

console.dir(x()());

//==================================================
//==================================================
// usecase
[1,2,3,4].filter(n => n > 2).map(n => n * n);
