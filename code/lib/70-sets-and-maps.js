//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map
// Maps
clear();
var myMap = new Map();
// set - Type specific (unlike objects)
myMap.set(true, "Boolean True");
myMap.set("true", "String True");

// get
console.log(myMap.get(true));
console.log(myMap.get("true"));

// vs. objects
var myObj = {
    true: "Boolean true??",
    "true": "String true"
}

// get
console.log("Huh! " + myObj[true]);
console.log("Really? " + myObj["true"]);

// sets
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set

clear();
var mySet = new Set();
// set - Type specific (unlike objects)
mySet.add(true);
mySet.add("true");
mySet.add(true);

console.log("Size: " + mySet.size);

// get
console.log(mySet.has(true));
console.log(mySet.has("true"));

// weakhashmaps
// https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/WeakMap

// keys HAVE TO BE non-null objects!
// Great for storing metadata for objects
clear();

// declare a map of users to passwords
const userPwds = new WeakMap();

function User(userSuppliedPwd) {
    const attrs = { pwd: userSuppliedPwd };
    userPwds.set(this, attrs);
}

User.prototype.authenticate = function(pwd) {
    const storedPwd = userPwds.get(this);
    return pwd == storedPwd.pwd;
}

let raju = new User("password");
console.dir(raju);
console.log("Can I authenticate? " + raju.authenticate("password"));
// if the user gets deleted
//raju = null;
// the weak map will 'eventually' drop the associated password
console.log(userPwds.has(raju));

// DOES NOT WORK WITH ANY BROWSER
// Just like WeakMaps you can ONLY add Objects
let ws = new WeakSet();
let obj = {};

ws.add(obj);
ws.has(obj); // returns true

ws.delete(obj);

ws.clear();
