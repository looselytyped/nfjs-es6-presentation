// Array comprehension syntax
[for (i of [1,2,3]) (i * 2)]; // [2,4,6]

// equivalent map
[1,2,3].map((n) => n * 2); // [2,4,6]

// supports 'if' conditional
[for (i of [1,2,3]) if (i > 1) i] // [2,3]

// equivalent filter
[1,2,3].filter((n) => n > 1);

// multiple arrays (goes left to right)

let cards = Array.apply(null, Array(10)).map((_, i) => (i+1)+"");

cards.push("J", "Q", "K");

let suits = ["Club", "Diamond", "Heart", "Spade"];

[for (c of cards) for (s of suits) (c+'-'+s)];
// ["1-Club", "1-Diamond", "1-Heart", "1-Spade",
//  "2-Club", "2-Diamond", "2-Heart", "2-Spade",
//  "3-Club", "3-Diamond", "3-Heart", "3-Spade", ... ]
