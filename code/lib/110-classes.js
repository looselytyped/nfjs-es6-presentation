class Todo {
    constructor(text) {
        this.text = text;
    }

    toString() {
        return `You should ${this.text}`;
    }

    get isDone() {
        this.done
    }
}

(function() {
    let t = new Todo("Buy Milk");
})();

(function() {
    let x = new MyList(3,4,5);
    console.log(x)
})();

class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    toString() {
        return `(${this.x}, ${this.y})`;
    }
}

class ColorPoint extends Point {
    constructor(x, y, color) {
        super(x, y); // (A)
        this.color = color;
    }
    toString() {
        return super.toString() + ' in ' + this.color; // (B)
    }
}
