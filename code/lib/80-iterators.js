//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Iterators_and_Generators

// creates an iterator over collections
// returns an array of [key, value] by default
clear();
let arr = [10, 20, 30];
let iter = Iterator(arr);

console.log(iter.next()); // [0,10]
console.log(iter.next()); // [1,20]
console.log(iter.next()); // [2,30]

//==================================================
// the for .. in loop automatically invokes 'next'
clear();
let arr = [10, 20, 30];
let iter2 = Iterator(arr);

for(let i in iter2) {
    console.log(i);
}

//==================================================
// works for objects too
let obj = {
    name: 'raju',
    age: 34
}

let iter3 = Iterator(obj);
// ["name", "raju"]
// ["age", 34]
for(let i in iter3) {
    console.log(i);
}

//==================================================
// works for maps and sets
let m = new Map().put("a", 1).put("b", 2);

//==================================================
// you can define custom iterators for your own
// objects by tacking a __iterator__ property on it
// The __iterator__ should be itself an function that
// returns an object that responds to the 'next' call
clear();


var iterableObj = {
    upperLimit: 10,
    '@@iterator':  function() {
        // __iterator__ is a function
        var that = this;
        // that returns an object
        return {
            cur: 0,
            // that has the next function defined
            next: function() {
                if(this.cur < that.upperLimit) {
                    let ret = this.cur;
                    this.cur++;
                    return {value: ret, done: false};
                }
                this.cur = 0;
                return {done: true};
            }
        }
    }
}

for(let i of iterableObj) {
    console.log(i);
}

