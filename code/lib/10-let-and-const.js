'use strict';

//==================================================
//==================================================
// let variables are only hoisted to the top of their enclosing block
(function withLet() {
    console.log('Before Block: a is ' + a);
    //console.log('Before Block: b is ' + b); // ReferenceError
    if (true) {
        var a = 1;
        let b = 2;
        console.log('Inside Block: a is ' + a);
        console.log('Inside Block: b is ' + b);
    }
    console.log('After Block: ' + a); // 1
    //console.log('After Block: ' + b); // ReferenceError: b is not defined
})();

//==================================================
//==================================================
// const variables are unchangeable
(function withConst() {
    console.log('Before Block: a is ' + a);
    if (true) {
        var a = 1;
        const b = 2;
        console.log('Inside Block: a is ' + a);
        console.log('Inside Block: b is ' + b);

        //const b = 3; // TypeError
        //b = 4; // Does NOT ERROR, but also does NOTHING
    }
    console.log('After Block: ' + a); // 1
})();
