// Parameters can have default values
function defaultOne(x = 1, y = 2){
    return [x, y];
}

console.log("No args: " + defaultOne());
console.log("x = 10: " + defaultOne(10));
console.log("Both args: " + defaultOne(10, 20));

//==================================================
//==================================================
// parameters can see values of preceding parameters
function defaultOne(x = 1, y = x * 2){
    return [x, y];
}

console.log("No args: " + defaultOne())
console.log("x = 10: " + defaultOne(10));
console.log("Both args: " + defaultOne(10, 30));
