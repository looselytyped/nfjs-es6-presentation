//==================================================
//==================================================
// Destructuring
let point = { x: 1, y: 2 };
let {x: a, y: b} = point;

print(a); // 1
print(b); // 2

// object
let point = { x: 1, y: 2 };
let {x, y} = point;

print(x); // 1
print(y); // 2

// array
let [x, y] = [1,2];
print(x); // 1

// nested works just as well
let [[, c], y] = [['a', 'b'],2];
print(c); // b

//==================================================
//==================================================
// easy swap
let x = 10, y = 20;
[y, x] = [x, y];
print(x); // 20
print(y); // 10

//==================================================
//==================================================
// argument destructuring works as well
function destruct({name, age}) {
    return "My name is " + name + " and I am " + age;
}

let me = { name: "Raju", age: 34 };
print(destruct(me)); // My name is Raju and I am 34

//==================================================
//==================================================
// destructuring in forEach (short version)
let students = [ {name: 'raju', age: 34 }, { name: 'bob', age: 25 } ];
students.forEach(({name, age}) => print(name+' '+age));

// destructuring in forEach (long version)
let students = [ {name: 'raju', age: 34 }, { name: 'bob', age: 25 } ];
students.forEach(({name:n, age:a}) => print(n+' '+a));


//==================================================
//==================================================
// destructuring with regex
let PHONE_MATCH  = /^(\d{3})-(\d{3})-(\d{4})$/;
let [, city, area, local]  = PHONE_MATCH.exec("614-555-1234");
print([city, area, local]);
