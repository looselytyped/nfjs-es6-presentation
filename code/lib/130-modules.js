// EXPORTING

//////////////////////////
// inline exporting
//////////////////////////

// string-utils.js
export function encrypt(msg) {
    return msg.split('').reverse().join('');
}

export const MY_CONSTANT = "Some Constant";
var someOtherVar = "Is not exported";

//////////////////////////
//  exporting at the end
//////////////////////////

// string-utils.js
function encrypt(msg) {
    return msg.split('').reverse().join('');
}

const MY_CONSTANT = "Some Constant";
var someOtherVar = "Is not exported";

export { encrypt, MY_CONSTANT };

// OR rename

export { encrypt as doNotTrackMe, MY_CONSTANT as SOME_CONSTANT };

// OR

module.exports = {
    encrypt: doNotTrackMe,
    MY_CONSTANT: SOME_CONSTANT;
}


// In production loading files by name does not work since
// most minify and concatenate js files. So rather than
// relying on a name of a file you can export a module like so

module "stringUtils" {
    export function encrypt(msg) {
        return msg.split('').reverse().join('');
    }

    export const MY_CONSTANT = "Some Constant";
    var someOtherVar = "Is not exported";
}

// IMPORTING

// in your main.js
import { doNotTrackMe } from 'string-utils';

// if there are a lot of exports from a file you can
// import them all together as

// everything gets tacked on global ns
import * from 'string_utils.js';

// to avoid that you can do
import 'string_utils.js' as str;

// or import only certain "exports"
import { doNotTrackMe } from 'string_utils.js';

// rename the import
import { doNotTrackMe as myEncrypt } from 'string_utils.js';

// Programmatic API

// allows conditionals
System.import(
    ['stringUtils'],
    function encryptPassword(stringUtils) {
        // use stringUtils.encrypt here
    }
)
