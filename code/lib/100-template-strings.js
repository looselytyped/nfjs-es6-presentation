// Uses back ticks

// Simple substitution

let name = 'Raju';
let greeting = `Hola! My name is ${name}`;

// can be any expression
let loudGreetings = `HOLA! MY NAME IS ${name.toUpperCase()}`;

// multi-line strings
let longStr = `This is a
doc string can be a multi-line
string
`


// template strings are IMMEDIATELY evaluated.
// So `name` should exist in the current scope!
// Cannot be externalized for the very same reason

// template 'handler' functions

let firstName = 'raju';
let lastName = 'gandhi';

function handler(str, ...subs) {
    // Gets the "raw" string as an array that looks like
    // ["Hello", " ", "!"]

    // This array has a "raw" property that gives you the
    // String as is (e.g for `Hello \n` the "raw" will be
    // ["Hello \\n"], whereas str will be ["Hello \n"]
    // You can get to it by "str.raw"

    // The substitutions are passed in as the remaning args
    console.log(subs[0]); // "raju"
    console.log(subs[1]); // "gandhi"
}

// This is NOT a regular function call!
handler `Hello ${firstName} ${lastName}!`

// Can be used for
// L10N
myButton.innerText = msg`Open`;

// Dynamic regex generation
re`\d+(${localeSpecificDecimalPoint}\d+)?`
