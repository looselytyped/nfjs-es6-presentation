//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Iterators_and_Generators

//==================================================
clear();

function* someGenerator() {
    yield 1;
    yield 2;
    yield 3;
}

let gen = someGenerator();
print(gen.next().value); //1
print(gen.next().value); //2
print(gen.next().value); //3
print(gen.next().value); //undefined


//==================================================
clear();


function* range(start = 0, end = Number.POSITIVE_INFINITY, step = 1) {
    while(start < end) {
        let ret = start;
        start = start + step;
        yield ret;
    }
    throw StopIteration;
}

let range = range()
console.log(range.next().value);
console.log(range.next().value);
console.log(range.next().value);
