#!/bin/bash

# wget https://archive.mozilla.org/pub/firefox/nightly/latest-mozilla-central/jsshell-mac.zip;
# rm js libmozglue.dylib libnss3.dylib;
# tar -xvf jsshell-mac.zip;
# rm jsshell-mac.zip;

wget https://archive.mozilla.org/pub/firefox/nightly/latest-mozilla-central/jsshell-mac.zip;
if [ $? -eq 0 ]
  then
  rm js libmozglue.dylib libnss3.dylib;
  tar -xvf jsshell-mac.zip;
  rm jsshell-mac.zip;
else
  echo "There was an issue with the download. Leaving everything as is"
  exit 1
fi
