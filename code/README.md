# playing-with-es-2015 

> A test bed to play with new features in ES2015 and transpile via Babel


## Usage

```sh
npm install

```

## License

MIT © [Raju Gandhi](www.rajugandhi.com)
