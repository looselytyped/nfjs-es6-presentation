# ES Next

This is my presentation for NFJS. 

## Usage

The easiest way to use the code in this demo is to use `spider-monkey`. 
Under `code/jsshell-mac` there is a script named `get-latest.sh` which will fetch the latest build from `mozilla.org`. 

Simply run `./get-latest.sh` in that directory and you will have the executable. 

### Running the code

Edit any file under `code/lib`, then invoke `<path-to-current-dir>/code/jsshell-mac/js <path-to-file>`

If you are using Sublime Text then 

- First install the Sublime Text alias as described [here](http://olivierlacan.com/posts/launch-sublime-text-3-from-the-command-line/)
- **Bear** in mind I call my shortcut `subl`
- Then `cd` to `<path-to-this-project>/code`, and run `subl -p playing-with-es6.sublime-project`
- Under Tools you should now have a new menu item called `Run with SpiderMonkey`. 
  Select that, and then a `⌘ + B` will run the currently open file against SpiderMonkey and display the output in the output window

  Enjoy!



- Open Presentation
- Open code in Sublime Text
- [Kangax](http://kangax.github.io/es5-compat-table/es6/)
- [Exploring ES6](http://exploringjs.com/es6/)
- [Understanding ECMAScript 6](https://leanpub.com/understandinges6/read/)
- [Babel](http://babeljs.io/repl/#?experimental=true&evaluate=true&loose=false&spec=true&code=)
